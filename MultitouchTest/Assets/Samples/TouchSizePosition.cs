﻿using Multitouch.EventSystems.EventData;
using Multitouch.EventSystems.Gestures;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Scripts
{
    [RequireComponent(typeof(RectTransform))]
    public class TouchSizePosition : UIBehaviour, 
        IPanHandler, IPinchHandler, IRotateHandler,
        IPointerDownHandler, IPointerClickHandler
    {
        public Vector2 MinimumSize = new Vector2(100f, 100f);
        public Vector2 MaximumSize = new Vector2(2048f, 2048f);
        public float PinchFactor = 1.5f;

        protected override void Start()
        {
            base.Start();
            var rcTransform = (RectTransform)transform;
            _originalSize = rcTransform.rect.size;
            _originalRotation = rcTransform.rotation;

        }

        public void OnPan(SimpleGestures sender, MultiTouchPointerEventData eventData, Vector2 delta)
        {
            var rcTransform = GetTransformAndSetLastSibling();

            TranslateByScreenDelta(rcTransform, eventData.delta, eventData.pressEventCamera);
            eventData.eligibleForClick = false;
        }

        public void OnPinch(SimpleGestures sender, MultiTouchPointerEventData eventData, Vector2 pinchDelta)
        {
            var rcTransform = GetTransformAndSetLastSibling();

            var size = rcTransform.rect.size;
            float ratio = Mathf.Sign(pinchDelta.x) * (pinchDelta.magnitude / size.magnitude);
            size *= 1.0f + ratio * PinchFactor;
            size = Vector2.Max(size, MinimumSize);
            size = Vector2.Min(size, MaximumSize);
            rcTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);
            rcTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
        }

        public void OnRotate(SimpleGestures sender, MultiTouchPointerEventData eventData, float delta)
        {
            var rcTransform = GetTransformAndSetLastSibling();

            rcTransform.Rotate(new Vector3(0, 0, -delta));
        }


        public void OnPointerDown(PointerEventData eventData)
        {
            eventData.Use();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            var rcTransform = GetTransformAndSetLastSibling();
            rcTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _originalSize.x);
            rcTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _originalSize.y);
            rcTransform.rotation = _originalRotation;
        }

        private RectTransform GetTransformAndSetLastSibling()
        {
            var rcTransform = (RectTransform)transform;
            rcTransform.SetAsLastSibling();
            return rcTransform;
        }

        public static void TranslateByScreenDelta(RectTransform rc, Vector2 delta, Camera cam)
        {
            var pos = rc.anchoredPosition;
            var parent = (RectTransform)rc.parent;
            var worldPos = parent.TransformPoint(pos);
            var screenPoint = RectTransformUtility.WorldToScreenPoint(cam, worldPos);
            screenPoint += delta;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parent, screenPoint, cam, out pos);
            rc.anchoredPosition = pos;
        }

        private Vector2 _lastSize;
        private Vector2 _originalSize;
        private Quaternion _originalRotation;
    }
}
